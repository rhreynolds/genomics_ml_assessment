# CoSyne Therapeutics Machine Learning Assessment in Genomics

This repo contains the take home assessment for the Machine Learning Engineer position at CoSyne Therapeutics. The assessment is to build a model to elucidate the relationship between certain gene features and the response to interventions on cell lines.

The task is to write a model that will uncover these relationships and to write some terraform/cloudformation config to deploy the model to AWS.

## Data
There are two data files in /data

1. features.csv 
2. dependency.csv


features.csv is the list of genetic features along with the number of copy number variants for each cell line, where ‘0’ is a loss of that gene, 1’ is a single copy, ‘2’ is two copies and so on. 

dependency.csv is the list of ‘interventions’ and their associated response where ‘0’ is no response and ‘1’ is a positive response

Along the top of the data frame are the cell lines used these start with ‘ACH’ and should correspond to each other in the two data frames.
This data is real world cancer genomics data and although some effort has been made to process this dataset, there will be some issues in the data.
These issues could be missing values, highly correlated features and other things that are typically found in real biological data

For more details on the data, please see the data_introspection.ipynb notebook.
## The task

Your task is to write a model/algorithm that will demonstrate how the features relate to the outcomes of each intervention in each cell line.

So for each intervention, we would like to know which features corresponded with a positive outcome.

The output of the assessment should be

1. A trained model that correlates interventions with features
2. A file that shows the relative feature importance for each intervention
3. A cloud infrastructure design for the training/deployment of the model, ideally in terraform/cloudformation or similar
4. A short write-up containing the results and a brief explanation of the model you chose, the pros/cons along with the infrastructure design

# The Output

The output should be a file which shows a ranked list of feature importance for each intervention that causes a positive outcome in the cell lines in the format

1. INT1,FEAT1,FEAT2,FEAT3 ...
2. INT2,FEAT4,FEAT5,FEAT6 ...

Along with this, please also send the code you used to generate this list, any saved model files (if applicable)
Please also send over a write up (pdf/readme/notebook) that explains your model choice and any results you found

Please also send over a yaml/json file containing terraform/cloudformation code which allows a model to be trained on a cloud provider.

# What we are looking for

It is important to know that the results you generate in this assessment are not what we are looking to judge a candidate on.

What we will be looking at is:

1. The choice of model used (is it appropriate?)
2. The reasoning you had for that choice
3. The quality of the code/implementation
4. The quality and suitability of the cloud infrastructure template you generate. (You do not need to deploy this yourself, just an infrastructure design that makes sense is all we need). 